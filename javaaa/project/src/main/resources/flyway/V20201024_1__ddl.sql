create table auth
(
    auth_id  serial not null
        constraint auth_pk
            primary key,
    id       bigint
        constraint auth_shops_id_fk
            references shops,
    login    varchar default 255,
    password varchar default 255
);

alter table auth
    owner to postgres;

