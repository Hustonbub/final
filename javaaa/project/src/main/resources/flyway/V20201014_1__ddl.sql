create table if not exists type
(
    id          serial       not null
        constraint type_pk
            primary key,
    name        varchar(255) not null,
    description varchar(255) not null
);

alter table type
    owner to postgres;

create table  if not exists bread
(
    id      serial           not null
        constraint bread_pk
            primary key,
    name    varchar(255)     not null,
    type_id integer           not null
        constraint bread_fk0
            references type,
    price   double precision not null,
    weight  double precision not null
);

alter table bread
    owner to postgres;

create table  if not exists shops
(
    id   serial       not null
        constraint "Shops_pk"
            primary key,
    name varchar(255) not null
);

alter table shops
    owner to postgres;

create table  if not exists orders
(
    id      serial           not null
        constraint orders_pk
            primary key,
    shop_id integer          not null
        constraint orders_fk0
            references shops,
    price   double precision not null,
    date    date             not null
);

alter table orders
    owner to postgres;

create table  if not exists order_item
(
    id          serial           not null
        constraint order_item_pk
            primary key,
    order_id    integer          not null
        constraint order_item_fk0
            references orders,
    bread_id    integer          not null
        constraint order_item_fk1
            references bread,
    quantity    integer          not null,
    total_price double precision not null
);

alter table order_item
    owner to postgres;

