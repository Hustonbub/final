INSERT INTO public.type (id, name, description) VALUES (1, 'Baguette', ' a French bread with a chewy crust and light interior.');
INSERT INTO public.type (id, name, description) VALUES (2, 'Brioche', 'a yeast bread packed with butter and eggs.');
INSERT INTO public.type (id, name, description) VALUES (3, 'Challah', 'a traditionally Jewish, braided bread made with eggs.');
INSERT INTO public.type (id, name, description) VALUES (4, 'Ciabatta', 'an Italian bread ideal for sandwiches and paninis.');
INSERT INTO public.type (id, name, description) VALUES (5, 'Cornbread', 'a quick bread made with eggs.');
INSERT INTO public.type (id, name, description) VALUES (6, 'Focaccia', ' an Italian bread baked flat in an oven.,');
INSERT INTO public.type (id, name, description) VALUES (7, 'Multigrain', 'a bread made with two or more grains.');
INSERT INTO public.type (id, name, description) VALUES (8, 'Pita Bread', 'a flatbread that originated in the Middle East.');
INSERT INTO public.type (id, name, description) VALUES (9, 'Potato Bread', ' a leavened or unleavened bread made from potato flour.');
INSERT INTO public.type (id, name, description) VALUES (10, 'Pumpernickel', 'a slightly sweet, rye bread that originated in Germany.');
INSERT INTO public.type (id, name, description) VALUES (11, 'Rye bread', ' a dense bread that can be light, medium, or dark in color');
INSERT INTO public.type (id, name, description) VALUES (12, 'Soda bread', 'a quick bread made with baking soda as a leavening agent.');
INSERT INTO public.type (id, name, description) VALUES (13, 'Sourdough', 'a bread made by a fermenting process using specific ingredients.');
INSERT INTO public.type (id, name, description) VALUES (14, 'Tortilla', 'a thin flatbread made from wheat or corn.');
INSERT INTO public.type (id, name, description) VALUES (15, 'Whole Wheat', 'a brown bread made using flour from wheat grains.');


INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (1, 'Baguette', 1, 2, 200);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (2, 'Brioche', 2, 3, 100);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (3, 'Challah', 3, 2, 200);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (4, 'French Baguette', 1, 5, 300);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (5, 'Ciabatta', 4, 3, 200);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (6, 'Focaccia', 6, 2, 200);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (7, 'Multigrain', 7, 2.60000000000000009, 160);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (8, 'Corn bread', 5, 3.5, 120);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (9, 'Pita Bread', 8, 2.10000000000000009, 110);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (10, 'Potato bread', 9, 2.20000000000000018, 120);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (11, 'Classic corn bread', 5, 3.39999999999999991, 115);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (12, 'Pumpernickel', 10, 2.70000000000000018, 110);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (13, 'Rye bread', 11, 2.70000000000000018, 110);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (14, 'Soda bread', 12, 2.89999999999999991, 190);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (15, 'Tortilla', 14, 2.85999999999999988, 120);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (16, 'Sourdough', 13, 2.66000000000000014, 130);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (17, 'French Tortilla', 14, 3.77000000000000002, 200);
INSERT INTO public.bread (id, name, type_id, price, weight) VALUES (18, 'Whole Wheat', 15, 2.56000000000000005, 122);

INSERT INTO public.shops (id, name) VALUES (1, 'Inabat');
INSERT INTO public.shops (id, name) VALUES (2, 'Ayanat');
INSERT INTO public.shops (id, name) VALUES (3, 'Voyage');
INSERT INTO public.shops (id, name) VALUES (4, 'Solnechnyi');
INSERT INTO public.shops (id, name) VALUES (5, 'Emir');
INSERT INTO public.shops (id, name) VALUES (6, 'Arman');
INSERT INTO public.shops (id, name) VALUES (7, 'Dos');
INSERT INTO public.shops (id, name) VALUES (8, 'Jubileinyi');
INSERT INTO public.shops (id, name) VALUES (9, 'Berezka');
INSERT INTO public.shops (id, name) VALUES (10, 'Sabrina');


INSERT INTO public.orders (id, shop_id, price, date) VALUES (8, 8, 120000, '2020-08-10');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (9, 9, 11000, '2020-08-10');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (10, 10, 1200000, '2020-08-07');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (1, 1, 200, '2020-10-13');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (2, 2, 600, '2020-08-07');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (3, 3, 600, '2020-09-08');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (4, 4, 100, '2020-08-07');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (5, 5, 300, '2020-08-06');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (6, 6, 800, '2020-08-07');
INSERT INTO public.orders (id, shop_id, price, date) VALUES (7, 7, 130, '2020-08-08');

