var app = angular.module('aitu-project', []);

app.controller('ProductCtrl', function($scope, $http) {
    $scope.login = {};
    $scope.productList = [];
    $scope.orderItemList = {};
    $scope.orderitemList = [];
    $scope.categoryName = [];


    $scope.getProducts = function () {
        $http({
            url: 'http://localhost:8083/bread',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.productList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getTypes = function () {
        $http({
            url: 'http://localhost:8083/type',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.typeList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getBreadsByTypes = function (type_id) {
        $http({
            url: 'http://localhost:8083/bread/type/' + type_id,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.productList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getTypes();
    $scope.getProducts();


    $scope.auth = {
        login: '',
        password: ''
    };
    $scope.registration = function (auth) {
        console.log(auth);
        $http({
            url: 'http://localhost/registration',
            method: 'POST',
            data: {
                'login': auth.login,
                'password': auth.password
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function(response){
            console.log("SUCCESS");
            console.log(response);
            $scope.signin = response.data;
            $scope.me();
        }, function (response) {
            console.log("ERROR");
            $scope.signin = {};
            console.log(response);
            $scope.me();
        })
    };


    $scope.login = function (auth){


    $http({
            url: 'http://localhost:8083/login',
            method: "POST",
        data: {
            'login': auth.login,
            'password': auth.password
        },
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json"
        }
    }).then(function (response){
        console.log("SUCCESS");
        console.log(response);
        $scope.login = response.data;
        $scope.me();

    }, function (responsed){
        console.log("ERROR");
        $scope.login= {};
        console.log(response);
        $scope.me();
    })

    };
    $scope.customerMessage = '';
    $scope.shop = {};


    $scope.getMe = function () {
        $http({
            url: 'http://localhost:8083/shops/me',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "",
                "Content-Type": "application/json",
                "Auth": $scope.login.token
            }
        })
            .then(function (response) {
                if($scope.login.id==1){
                    window.location.replace("http://localhost:63342/demo/templates/factory.html");

                }
                else{
                    $scope.shop = response.data;
                    $scope.shopMessage = 'Hello ';}
            }, function (response){
                $scope.shop = {};
                $scope.shopMessage = 'Login or Password is incorrect!';
            })


    };

    $scope.alter = function (){
        $scope.breadID = [];
        $scope.breadQuantity = [];
        $scope.breadPrice = [];
        angular.forEach($scope.orderItemList, function (value){
            $scope.breadID.push(value.id);
            if ($scope.breadQuantity[value.id] === undefined) {
                $scope.breadQuantity[value.id] =  value.quantity;
            }
            if ($scope.breadPrice[value.id] === undefined) {
                $scope.breadPrice[value.id] =  value.price;
            }
        });
    }

    $scope.createOrder = function (token) {
        $http({
            url: 'http://127.0.0.1:8083/order',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "shop_id": $scope.shop.id,
            }
        }).then(function (response){
            $scope.token = response.data.token;
            console.log($scope.token);
            $scope.createOrderItem($scope.login.token);
            $scope.orderItemList = {};
        }, function (response){
            console.log(response);
        })
    }
    $scope.createOrderItem = function (token) {
        $scope.alter();
        $http({
            url: 'http://127.0.0.1:8083/save/order_item',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "shop_id": $scope.shop.id,
                "product_id": $scope.breadID,
                "quantity": $scope.breadQuantity,
                "price": $scope.breadPrice
            }
        }).then(function (response){
            console.log(response);
        }, function (response){
            console.log(response);
        })
    }

    let orderItemList = {};

    $scope.incrementProduct= function (bread){
        if ($scope.orderItemList[bread.id] === undefined) {
            $scope.orderItemList[bread.id] =  {id: bread.id, category_id:bread.category_id,name: bread.name,price: bread.price, description: bread.description,  quantity: 0};
        }
        $scope.orderItemList[bread.id].quantity = $scope.orderItemList[bread.id].quantity + 1;
    }

    $scope.decrementProduct = function (bread) {
        if ($scope.orderItemList[bread.id] === undefined) {
            $scope.orderItemList[bread.id] =  {id: bread.id, category_id:bread.category_id,name: bread.name,price: bread.price, description: bread.description,  quantity: 0};
        }
        $scope.orderItemList[bread.id].quantity = $scope.orderItemList[bread.id].quantity - 1;

    };

    $scope.getOrderList = function(token) {
        $http({
            url: 'http://127.0.0.1:8083/order_item/auth',
            method: "GET",
            headers: {
                "shop_id":$scope.shop.id,
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.orderItemList = response.data;
                },
                function (response) {
                    console.log(response);
                });};

    $scope.SendOrder = function () {
        if (Object.keys($scope.login).length === 0) {
        } else {
            if (Object.keys($scope.orderItemList).length === 0){
            } else {
                $scope.createOrder($scope.token);
                console.log($scope.login);
            }
        }
   /*$scope.sendOrders = function () {
        console.log(orderItemList);

        let totalPrice = 0;
        angular.forEach(orderItemList, function (value) {
            totalPrice += value.price * value.quantity;
        });
        console.log(totalPrice);*/
    }
    $scope.alter = function (){
        $scope.breadID = [];
        $scope.breadQuantity = [];
        $scope.breadPrice = [];
        angular.forEach($scope.orderItemList, function (value){
            $scope.breadID.push(value.id);
            if ($scope.breadQuantity[value.id] === undefined) {
                $scope.breadQuantity[value.id] =  value.quantity;
            }
            if ($scope.breadPrice[value.id] === undefined) {
                $scope.breadPrice[value.id] =  value.price;
            }
        });
    }


    $scope.ShowOrder = function () {
        if (Object.keys($scope.login).length === 0) {
        } else {
            $scope.getOrderList($scope.login.token);
        }
    }
});




