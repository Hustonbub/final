var app = angular.module('aitu-project', []);

app.controller('FactoryCtrl', function($scope, $http) {

    $scope.login = {};
    $scope.OrderStatusList = [];
    $scope.statusName = [];
    $scope.orderStatusChange = {};
    $scope.shop = {};
    $scope.dashboard = [];


    $scope.getDashboard = function () {
        $http({
            url: 'http://127.0.0.1:8083/order',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },

        }).then(function (response){
            $scope.dashboard = response.data.token;
            console.log($scope.token);

        }, function (response){
            console.log(response);
        })
    }



    $scope.login = function (){
        $http({
            url: 'http://127.0.0.1:8083/login/1',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.login = response.data;

        }, function (response){
            $scope.login = {};
            console.log(response);
        })
    };

    $scope.getStatus = function() {
        $http({
            url: 'http://127.0.0.1:8083/status',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.statusName = response.data;
                },
                function (response) {
                    console.log(response);
                });
    };
    $scope.getStatus();

    $scope.getOrderStatus = function(status) {
        $http({
            url: 'http://127.0.0.1:8083/order/status/'+ status,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
        })
            .then(function (response) {
                    console.log(response);
                    $scope.OrderStatusList = response.data;
                },
                function (response) {
                    console.log(response);
                });
    };




    $scope.changeStatus1 = function(id,status) {
        if(status<$scope.statusName.length+1){

            $http({
                url: 'http://127.0.0.1:8083/order/update',
                method: "POST",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                    "order_id":id,
                    "status":status
                },

            })
                .then(function (response) {
                        console.log(response);

                    },
                    function (response) {
                        console.log(response);
                    });
        }};

    $scope.changeStatus = function (order,status){
        if (order.id === undefined) {
            $scope.orderStatusChange[order.id] =  {id: order.id, shop_id:order.shop_id,date:order.date,total_price: order.total_price, status: order.status};
        }
        $scope.changeStatus1(order.id,status);
    }

});
