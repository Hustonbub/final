package com.example.project.repository;

import com.example.project.entity.Status;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatusRepository extends CrudRepository<Status, Long> {
    @Query(value = "SELECT name from status",nativeQuery = true)
    List<String> getNames();
}
