package com.example.project.controller;


import com.example.project.service.BreadService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class BreadController {
    private BreadService breadService;

    public BreadController(BreadService breadService) {
        this.breadService = breadService;
    }


    @GetMapping("/api/bread/type/{type_id}")
    public ResponseEntity<?> getAllByTypeId(@PathVariable("type_id") Long type_id) {
        return ResponseEntity.ok(breadService.getByTypeId(type_id));
    }

    @GetMapping("/api/bread")
    public ResponseEntity<?> getAllBread() {
        return ResponseEntity.ok(breadService.getAll());
    }

    @DeleteMapping("/api/bread/{id}")
    public void deleteBread(@PathVariable Long id) {
        breadService.delete(id);
    }
}
