package com.example.project.repository;

import com.example.project.entity.Auth;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends CrudRepository<Auth, Long> {


    Auth findByLoginAndPassword(String login, String password);


    Auth findByToken(String token);

    static void registration(@Param("login") String login,
                             @Param("password") String password) {

    }


}
