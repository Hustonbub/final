package com.example.project.controller;

import com.example.project.entity.Status;
import com.example.project.service.StatusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatusController {
    private final StatusService statusService;

    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    @GetMapping("/api/status")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(statusService.getAll());
    }
    @GetMapping("/api/status/{id}")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(statusService.getById(id));}
    @PostMapping("/api/status")
    public ResponseEntity<?> update(@RequestBody Status shop){
        return ResponseEntity.ok(statusService.update(shop));
    }

    @GetMapping("/api/status/name")
    public ResponseEntity<?> getNames() {
        return ResponseEntity.ok(statusService.getNames());

    }
}
