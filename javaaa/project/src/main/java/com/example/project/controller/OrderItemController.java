package com.example.project.controller;


import com.example.project.entity.Order_item;
import com.example.project.service.OrderItemService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderItemController {
    private OrderItemService orderItemService;

    public OrderItemController(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @GetMapping("/api/v2/order_item/{id}")
    public ResponseEntity<?> getOrderItem(@PathVariable Long id) {
        return ResponseEntity.ok(orderItemService.getById(id));
    }

    @GetMapping("/api/v2/order_item")
    public ResponseEntity<?> getAllOrders() {
        return ResponseEntity.ok(orderItemService.getAll());
    }

    @PostMapping("/api/v2/order_item")
    public ResponseEntity<?> saveOrder(@RequestBody Order_item order_item) {
        return ResponseEntity.ok(orderItemService.create(order_item));
    }

    @PutMapping("/api/v2/order_item")
    public ResponseEntity<?> updateOrder(@RequestBody Order_item order_item) {
        return ResponseEntity.ok(orderItemService.create(order_item));
    }

    @DeleteMapping("/api/v2/order_item/{id}")
    public void deleteOrder(@PathVariable Long order_item_ID) {
        orderItemService.delete(order_item_ID);
    }
}
