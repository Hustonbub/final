package com.example.project.service;

import com.example.project.entity.Type;
import com.example.project.repository.TypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeService {
    private final TypeRepository typeRepository;

    public TypeService(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    public List<Type> getAll() {
        return (List<Type>) typeRepository.findAll();
    }


    public Type getById(Long id) {
        return typeRepository.findById(id).orElse(null);
    }
/*
    public Type create(Type type) {
        return typeRepository.save(type);
    }

    public Type update(Type type) {
        return typeRepository.save(type);
    }

    public void delete(Long id) {
        typeRepository.deleteById(id);
    }*/
}
