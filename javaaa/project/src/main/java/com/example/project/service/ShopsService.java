package com.example.project.service;

import com.example.project.entity.Shops;
import com.example.project.repository.ShopsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopsService {
    public final ShopsRepository shopsRepository;

    public ShopsService(ShopsRepository shopsRepository) {
        this.shopsRepository = shopsRepository;
    }

    public List<Shops> getAll() {
        return shopsRepository.findAll();
    }

    public Shops getById(Long id) {
        return shopsRepository.findById(id).orElse(null);
    }


    public Shops create(Shops shops) {
        return shopsRepository.save(shops);
    }

    public Shops update(Shops shops) {
        return shopsRepository.save(shops);
    }

    public void delete(Long id) {
        shopsRepository.deleteById(id);
    }
}
