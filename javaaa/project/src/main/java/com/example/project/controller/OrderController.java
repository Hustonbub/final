package com.example.project.controller;

import com.example.project.entity.Orders;
import com.example.project.service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {
    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/api/order/{id}")
    public ResponseEntity<?> getOrder(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.getById(id));
    }

    @GetMapping("/api/order")
    public ResponseEntity<?> getAllOrders() {
        return ResponseEntity.ok(orderService.getAll());
    }


    @PutMapping("/api/order")
    public ResponseEntity<?> updateOrder(@RequestBody Orders orders) {
        return ResponseEntity.ok(orderService.create(orders));
    }

    @DeleteMapping("/api/order/{id}")
    public void deleteOrder(@PathVariable Long orderId) {
        orderService.delete(orderId);
    }
}
