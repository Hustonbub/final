package com.example.project.repository;

import com.example.project.entity.Order_item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository<Order_item, Long> {

    List<Order_item> findAll();

}
