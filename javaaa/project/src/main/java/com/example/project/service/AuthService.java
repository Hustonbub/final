package com.example.project.service;

import com.example.project.entity.Auth;
import com.example.project.entity.Shops;
import com.example.project.repository.AuthRepository;

import lombok.AllArgsConstructor;
import com.example.project.repository.ShopsRepository;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final ShopsRepository shopsRepository;

    @Transactional
    public  Auth login(Auth auth) throws Exception {

        Auth authDB = authRepository.findByLoginAndPassword(auth.getLogin(), auth.getPassword());

        if (authDB == null) {
            throw new Exception();

        }
        String token = UUID.randomUUID().toString();

        authDB.setToken(token);
        authRepository.save(authDB);
    return authDB;
    }
    public Shops getShopsByToken(String token) throws Exception {
        Auth authDB = authRepository.findByToken(token);
        return shopsRepository.findById(authDB.getId()).orElseThrow(ChangeSetPersister.NotFoundException::new);

    }
    public void register(Auth auth) {
        String login=auth.getLogin();
        String password=auth.getPassword();
        AuthRepository.registration(login,password);
    }



}






