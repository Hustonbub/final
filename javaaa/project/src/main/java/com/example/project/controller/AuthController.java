package com.example.project.controller;

import com.example.project.entity.Auth;
import com.example.project.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class AuthController {
    private AuthService authService;
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Auth auth) {
        System.out.println(auth);

        try {
            return ResponseEntity.ok(authService.login(auth));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
    @GetMapping("/shops/me")
    public ResponseEntity<?>me(@RequestHeader("Auth") String token) throws Exception {
        return ResponseEntity.ok(authService.getShopsByToken(token));
    }
    @RequestMapping(value = "/registration", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> registration(@RequestBody Auth auth) {
        try {
            return ResponseEntity.ok(authService.login(auth));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST, headers = "Accept=application/json")
    public void register(@RequestBody Auth auth)  {
        authService.register(auth);
    }


}
