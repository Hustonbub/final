package com.example.project.service;

import com.example.project.entity.Orders;
import com.example.project.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OrderService {
    public final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Orders> getAll() {
        return orderRepository.findAll();
    }

    public Orders getById(Long id) {
        return orderRepository.findById(id).orElse(null);
    }


    public Orders create(Orders orders) {
        return orderRepository.save(orders);
    }

    public Orders update(Orders orders) {
        return orderRepository.save(orders);
    }

    public void delete(Long id) {
        orderRepository.deleteById(id);
    }


}
