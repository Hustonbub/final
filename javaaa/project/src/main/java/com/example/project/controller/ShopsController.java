package com.example.project.controller;

import com.example.project.entity.Shops;
import com.example.project.service.ShopsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShopsController {

        private ShopsService shopsService;

    public ShopsController(ShopsService shopsService) {
        this.shopsService = shopsService;
    }

    @GetMapping("/api/v2/shops/{id}")
        public ResponseEntity<?> getShops(@PathVariable Long shopsId) {
            return ResponseEntity.ok(shopsService.getById(shopsId));
        }

        @GetMapping("/api/v2/shops")
        public ResponseEntity<?> getAllShops() {
            return ResponseEntity.ok(shopsService.getAll());
        }

        @PostMapping("/api/v2/shops")
        public ResponseEntity<?> saveShops(@RequestBody Shops shops) {
            return ResponseEntity.ok(shopsService.create(shops));
        }

        @PutMapping("/api/v2/shops")
        public ResponseEntity<?> updateShops(@RequestBody Shops shops) {
            return ResponseEntity.ok(shopsService.create(shops));
        }

        @DeleteMapping("/api/v2/shops/{id}")
        public void deleteShops(@PathVariable Long shopsId) {
            shopsService.delete(shopsId);
        }
}
