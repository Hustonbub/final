package com.example.project.service;

import com.example.project.entity.Bread;

import com.example.project.repository.BreadRepository;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class BreadService {
    public final BreadRepository breadRepository;

    public BreadService(BreadRepository breadRepository) {
        this.breadRepository = breadRepository;
    }



    public List<Bread> getAll() {
        return (List<Bread>) breadRepository.findAll();
    }
    public Bread save(Bread bread){
        return breadRepository.save(bread);
    }
    public List<Bread> getByTypeId(Long type_id){
        return  breadRepository.findAllByTypeId(type_id);
    }



    public Bread update(@RequestBody Bread bread) {
        return breadRepository.save(bread);
    }

    public void delete(Long id) {
        breadRepository.deleteById(id);
    }


}
