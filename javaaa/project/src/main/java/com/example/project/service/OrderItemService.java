package com.example.project.service;
import com.example.project.entity.Order_item;
import com.example.project.repository.OrderItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderItemService {
    public final OrderItemRepository orderItemRepository;

    public OrderItemService(OrderItemRepository orderItemRepository) {
        this.orderItemRepository = orderItemRepository;
    }

    public List<Order_item> getAll() {
        return orderItemRepository.findAll();
    }

    public Order_item getById(Long id) {
        return orderItemRepository.findById(id).orElse(null);
    }


    public Order_item create(Order_item order_item) {
        return orderItemRepository.save(order_item);
    }

    public Order_item update(Order_item order_item) {
        return orderItemRepository.save(order_item);
    }

    public void delete(Long id) {
        orderItemRepository.deleteById(id);
    }
}
