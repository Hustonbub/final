package com.example.project.controller;


import com.example.project.service.TypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TypeController {
    private TypeService typeService;

    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }

    @GetMapping("/api/type/{id}")
    public ResponseEntity<?> getType(@PathVariable Long id) {
        return ResponseEntity.ok(typeService.getById(id));
    }

    @GetMapping("/api/type")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(typeService.getAll());
    }
/*
    @PostMapping("/api/type")
    public ResponseEntity<?> saveType(@RequestBody Type type) {
        return ResponseEntity.ok(typeService.create(type));
    }

    @PutMapping("/api/type")
    public ResponseEntity<?> updateType(@RequestBody Type type) {
        return ResponseEntity.ok(typeService.create(type));
    }

    @DeleteMapping("/api/type/{id}")
    public void deleteType(@PathVariable Long id) {
        typeService.delete(id);
    }*/
}
