package com.example.project.repository;


import com.example.project.entity.Shops;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShopsRepository extends CrudRepository<Shops, Long> {


    List<Shops> findAll();
}
