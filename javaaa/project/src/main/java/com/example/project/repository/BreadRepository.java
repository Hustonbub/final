package com.example.project.repository;

import com.example.project.entity.Bread;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BreadRepository extends CrudRepository<Bread, Long> {
    @Query(value = "select * from bread where bread.type_id = ? " , nativeQuery = true)


    List <Bread> findAllByTypeId(Long type_id);
}
